// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert statements
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <algorithm>

#include "Collatz.hpp"

using namespace std;

// Initialize the lazy cache that gets updated as problems are solved
int lazyCache[100000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;

    assert(i > 0 && i < 1000000);
    assert(j > 0 && j < 1000000);

    // This makes sure we know our start and end points
    int b = min(i,j);
    int e = max(i,j);


    // Adjusts bounds based to theorem shown in class
    int m = (j / 2) + 1;
    if ( m > b) {
        b = m;
    }

    // Enumerate through every number in the range to look for
    // largest cycle length
    int result = 1;
    for (int num = b; num <= e; ++num) {
        int c = 1;
        long n = num;
        // Look for collatz cycle length
        while (n != 1) {
            assert(n > 0);
            // Check's cache for n to save some time
            if (n < 100000) {
                assert(n < 100000);
                if (lazyCache[n] != 0) {
                    c += lazyCache[n] - 1;
                    break;
                }
            }
            if (n % 2 == 0) {
                n = (n >> 1);
                ++c;
            }
            else {
                n = n + (n >> 1) + 1;
                c += 2;
            }
        }
        // Update the cache with cycle length of num that was found
        if (num < 100000) {
            assert(n < 100000);
            lazyCache[num] = c;
        }
        // Update result if higher cycle length is found
        if (c > result) {
            result = c;
        }
    }


    return make_tuple(i, j, result);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
