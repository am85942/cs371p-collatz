# CS371p: Object-Oriented Programming Collatz Repo

* Name: Abhinav Mugunda

* EID: am85942

* GitLab ID: am85942

* HackerRank ID: am85942

* Git SHA: 6380cd2796987647275d72e78c55af791faf000e

* GitLab Pipelines: https://gitlab.com/am85942/cs371p-collatz/-/pipelines

* Estimated completion time: 8.00

* Actual completion time: 6.00

* Comments: None
