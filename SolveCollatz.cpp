// --------------
// RunCollatz.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <cassert>  // assert statements
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

using namespace std;
int lazyCache[100000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;

    int b = min(i,j);
    int e = max(i,j);

    int m = (j / 2) + 1;
    if ( m > b){
        b = m;
    }

    int result = 1;
    for (int num = b; num <= e; ++num) {


        int c = 1;
        long n = num;
        while (n != 1){

            if (n < 100000){
                if (lazyCache[n] != 0){
                    c += lazyCache[n] - 1;
                    break;
                }
            }

            if (n % 2 == 0){
                n = (n >> 1);
                ++c;
            }
            else {
                n = n + (n >> 1) + 1;
                c += 2;
            }
        }
        if (num < 100000){
            lazyCache[num] = c;
        }
        if (c > result){
            result = c;
        }
    }


    return make_tuple(i, j, result);}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
         collatz_print(sout, collatz_eval(collatz_read(s)));}

// ----
// main
// ----

int main () {
    collatz_solve(cin, cout);
    return 0;}
