// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(150000, 200000)), make_tuple(150000, 200000, 383));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(513900, 599935)), make_tuple(513900, 599935, 452));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(512235, 10031)), make_tuple(512235, 10031, 470));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(170042, 99766)), make_tuple(170042, 99766, 383));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(20440, 690459)), make_tuple(20440, 690459, 509));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(521477, 79353)), make_tuple(521477, 79353, 470));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(721332, 9844)), make_tuple(721332, 9844, 509));
}

TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(294499, 77313)), make_tuple(294499, 77313, 443));
}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval13) {
    ASSERT_EQ(collatz_eval(make_pair(500000, 500001)), make_tuple(500000, 500001, 152));
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

TEST(CollatzFixture, solve1) {
    istringstream sin("581496 905446\n137485 236456\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "581496 905446 525\n137485 236456 443\n");
}

TEST(CollatzFixture, solve2) {
    istringstream sin("322858 377608\n421989 163886\n881101 412437\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "322858 377608 441\n421989 163886 449\n881101 412437 525\n");
}

TEST(CollatzFixture, solve3) {
    istringstream sin("466697 367589\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "466697 367589 449\n");

}
